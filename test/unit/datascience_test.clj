(ns datascience-test
  (:require [primercheck.datascience :as sut]
            [malli.core :as m]
            [clojure.string :as str]
            [clojure.test :as t]
            [clojure.edn :as edn]
            [tablecloth.api :as tc]))


(def sql-result-single
  (-> (slurp "test/resources/sql-result-single.edn")
      edn/read-string))

(t/deftest high-quality-genotypes
  (t/testing "Dataframe contains only high quality genotypes."
    (let [ds (-> (sut/result->dataset sql-result-single)
                 (sut/only-high-quality-genotypes))]
      (t/is (= (ds "filter")
               [["AC0"] ["AC0"]])))))

