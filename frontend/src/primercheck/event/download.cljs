(ns primercheck.event.download
  (:require [clojure.string :as str]
    [primercheck.datascience
     :refer [transform-nans dataset->sorted]]))

(def ^:private column-order
  '(:unit :gene :transcript :amplicon :primer
          :chromosome :start :stop :seqf :seqr :type
          :filter :position :reference :alternate
          :af_raw :af_female :af_male :af_popmax
          :af_nfe :af_nfe_female :af_nfe_male :af_nfe_bgr :af_nfe_nwe :af_nfe_onf :af_nfe_swe :af_nfe_seu :af_nfe_est
          :af_fin :af_fin_female :af_fin_male
          :af_amr :af_amr_female :af_amr_male
          :af_afr :af_afr_female :af_afr_male
          :af_eas :af_eas_female :af_eas_male :af_eas_jpn :af_eas_kor :af_eas_oea
          :af_sas :af_sas_female :af_sas_male
          :af_asj :af_asj_female :af_asj_male
          :af_oth :af_oth_female :af_oth_male))

(defn order-data [data]
  (->> data
    (mapv #(apply array-map (interleave column-order (map % column-order))))))

(defn datestring []
  (-> (js/Date.)
    (.toISOString)
    (str/replace #"[-T:Z\.]" "")))

(defn download-csv [state]
  (let [file-name (:file-name state)
        data (order-data (dataset->sorted (transform-nans (:dataset state))))
        csv-file-header "data:text/csv;charset=utf-8,"
        csv-header (->> data first keys (mapv name) (interpose "\t") str/join)
        csv-rows (->>
                   (rest data)
                   (map (comp
                          #(apply str (interpose "\t" %))
                          #(vals %)))
                   (interpose "\n" )
                   (apply str))
        csv-content (apply str (vec (interpose "\n" [csv-header csv-rows])))
        file-contents (str/join [csv-file-header csv-content])
        encoded-uri (js/encodeURI file-contents)
        link (js/document.createElement "a")]
    (.setAttribute link "href" encoded-uri)
    (.setAttribute link "download" (str file-name "_gnomad-matches_" (datestring) ".tsv"))
    (js/document.body.appendChild link)
    (.click link)))
