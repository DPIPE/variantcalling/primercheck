(ns primercheck.event.upload
  (:require-macros [cljs.core.async.macros :refer [go]])
  (:require [clojure.string :as str]
            [clojure.walk :as walk]
            [cljs.core.async :refer [<!]]
            [cljs-http.client :as http]
            [tech.v3.dataset :as tmd]
            [datascript.core :as ds]
            [goog.object :as gobj]))

(defn upload-file [event conn uri file]
  (let [db (ds/db conn)
        app (ds/entity db :system/app)
        file-name (gobj/get file "name")
        _ (ds/transact! conn [[:db/add (:db/id app) :file-name file-name]])
        _ (js/console.debug (str "Trying to upload " file-name " to server at " uri))]
    (go (let [response (<! (http/post uri
                                      {:multipart-params [["file" file]]
                                                         :with-credentials? true}))]
          (ds/transact! conn [[:db/add (:db/id app) :upload-response (:status response)]])))))
