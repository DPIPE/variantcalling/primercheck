(ns primercheck.event.fetch
    (:require-macros [cljs.core.async.macros :refer [go]])
  (:require [clojure.string :as str]
            [clojure.walk :as walk]
            [cljs.core.async :refer [<!]]
            [cljs-http.client :as http]
            [tech.v3.dataset :as tmd]
            [datascript.core :as ds]
            [goog.object :as gobj]
            [primercheck.datastore :refer [conn]]))

(defn fetch-result [uri]
  (let [db (ds/db conn)
        app (ds/entity db :system/app)
        app-id (:db/id app)]
    (go (let [{:keys [status body]} (<! (http/get (str "http://localhost:8080" uri) {:with-credentials? true}))]
          (case status
                200 (do
                     (js/console.log "Received dataset from server")
                     (ds/transact! conn [[:db/add (:db/id app) :dataset (tmd/->dataset body)]]))
                500 (js/console.error "Failed fetching processing results" body))))))
