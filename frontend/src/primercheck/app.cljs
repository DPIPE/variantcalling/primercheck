(ns primercheck.app
  (:require [primercheck.datastore :refer [conn]]
            [primercheck.core :as core]))

(defonce root (js/document.getElementById "app"))

(defn init! []
  (core/main conn root))
