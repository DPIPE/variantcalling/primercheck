(ns primercheck.sse
  (:require ["eventsource-client" :as esc]
            [clojure.string :as str]
            [goog.object :as gobj]
            [datascript.core :as ds]
            [primercheck.datastore :refer [conn app-db]]
            [primercheck.event.fetch :refer [fetch-result]]))

(defn handle-processing-status [data]
      (let [[status uri] (gobj/getValues (js/JSON.parse data))]
        (case status
              "done" (do
                       (ds/transact! conn [[:db/add (:db/id (app-db)) :processing? false]])
                       (fetch-result uri))
              "running"  (ds/transact! conn [[:db/add (:db/id (app-db)) :processing? true]]))))

(defn handler [^js event-object]
      (let [[id event data] (gobj/getValues event-object)
            id (js/parseInt id)]
        (cond
         (= event "server-message") (js/console.log [id event data])
         (= event "processing-status") (handle-processing-status data)
         (= event "heartbeat") (js/console.debug "Server ping")
         :else (js/console.debug [id event data]))))

(defn on-connect-handler []
      (ds/transact! conn [[:db/add (:db/id (app-db)) :connected? true]])
      (js/console.log "Stream connected"))

(defn on-disconnect-handler []
      (ds/transact! conn [[:db/add (:db/id (app-db)) :connected? false]])
      (js/console.log "Stream disconnected"))

;; Some relevant, available options:
;; (see: https://github.com/rexxars/eventsource-client/blob/main/src/types.ts)
;; {:url ""
;;  :onMessage ()
;;  :onConnect ()
;;  :onScheduleReconnect (fn [{:keys [delay] :as info}])
;;  :onDisconnect ()
;;  :headers {"" ""}
;;  :mode "cors | no-cors | same-origin"
;;  :credentials "include | omit | same-origin"
;;  :body any?
;;  :method "get | post | put | ..."
;;  :redirect "error | follow"
;;  :referrer ""}

(def client
     (esc/createEventSource #js {:url "http://localhost:8080/event"
                                      :credentials "include"
                                 ;; :mode "cors"
                                      :onConnect #(on-connect-handler)
                                      :onDisconnect #(on-disconnect-handler)
                                      :onClose #(js/console.log "Stream closed")
                                      :onScheduleReconnect (fn [reconnect-info]
                                                               (js/console.debug (str "Scheduled reconnect: " (gobj/get reconnect-info "delay") " ms")))
                                      :onMessage (fn [e]
                                                     (handler e))}))
