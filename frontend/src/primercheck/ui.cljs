(ns primercheck.ui
  (:require [clojure.core.match :refer [match]]
            [datascript.core :as ds]
            [tech.v3.dataset :as tmd]
            [phosphor.icons :as icons]
            [primercheck.datascience :refer [dataset->sorted]]))

(def processing-icon
   (icons/render (icons/icon :phosphor.fill/circle)))

(defn process-indicator [state]
      (let [connected? (:connected? state)
            processing? (:processing? state)]
        [:div {:style {:display "flex" :align-items "center"}}
         [:span#connection-status (cond
                 (and connected? processing?) "processing"
                 (and connected? (not processing?)) "connected"
                  :else "connecting")]
         [:div#process-indicator {:style {:width "1rem" :display "grid" :place-items "center"}}
                                 (icons/render (icons/icon :phosphor.fill/circle) {:width "1rem"
                                                                                   :class (when connected? "pulse")
                                                                                   :color (cond
                                                                                           (and connected? processing?) "#d59844"
                                                                                           (and connected? (not processing?)) "#40b240"
                                                                                           :else "#c75745")})]] ))

(defn upload-form [state]
  [:div
   [:label#xlsx-upload-label
    {:for "xlsx-upload-input"
     :class (when (or (:processing? state)
                      (not (:connected? state))) "disabled")}
    "Upload .xlsx"
    [:input#xlsx-upload-input
     {:disabled (or (:processing? state)
                    (not (:connected? state)))
      :type "file"
      :name "file"
      :accept ".xlsx"
      :on {:change [[:dom/disable]
                    [:dom/prevent-default]
                    [:http/upload-xlsx "http://localhost:8080/upload" :event/target.file]
                    [:dom/set-input-text ""]
                    [:dom/enable]]}}]]])

(defn download-button [state]
      [:div
       [:button#download-button
        {:on {:click [[:ui/download-csv state]]}}
        "Download .tsv"]])

(defn format-number
      "Use pattern matching to format number cells based on value.
  That is, instead of displaying fractional zeros or NaNs, we
  simply put a minus sign to indicate absence, and integer `0` to
  indicate, well, zero. In addition, we truncate values to the
  same amount of decimal places as in GnomAD browser."
      [n]
      (match [(js/Number.isNaN n) (js/Number.isInteger n) (zero? n)]
             [true _ _] "-"
             [false _ true] 0
             [false true _] n
             :else (.toFixed n 8)))

;; TODO Populate table programmatically, instead of this ad hoc mess
(defn data-view [dataset]
  (let [columns (-> dataset tmd/column-names)
        rows (dataset->sorted dataset)
        ;; TODO Update state with filtered rows by column
        ;; to trigger rerender
        ;;_ (js/console.log :columns columns)
        ;;_ (js/console.log :groups rows)
        ]
    [:table#data-view
     [:thead
      [:tr
       [:th "Unit"]
       [:th {:on {:click [[:ui/sort-column :gene]]}} "Gene"]
       [:th "Transcript"]
       [:th "Amplicon"]
       [:th "Primer"]
       [:th "Chr"]
       [:th "Start"]
       [:th "Stop"]
       [:th "Seq F"]
       [:th "Seq R"]
       [:th {:title "[E]xome/[G]enome"} "Type"]
       [:th {:title "High Quailty Genotype?"} "HQ"]
       [:th "Pos"]
       [:th "Ref"]
       [:th "Alt"]
       [:th "AF"]
       [:th "AF [F]"]
       [:th "AF [M]"]
       [:th "AF PMAX"]
       [:th "AF European (Non-Finnish)"]
       [:th "AF European (Non-Finnish) [F]"]
       [:th "AF European (Non-Finnish) [M]"]
       [:th "AF Bulgarian"]
       [:th "AF North-Western European"]
       [:th "AF Other European"]
       [:th "AF Swedish"]
       [:th "AF Southern European"]
       [:th "AF European Estonian"]
       [:th "AF Finnish"]
       [:th "AF Finnish [F]"]
       [:th "AF Finnish [M]"]
       [:th "AF Admixed American"]
       [:th "AF Admixed American [F]"]
       [:th "AF Admixed American [M]"]
       [:th "AF African/African American"]
       [:th "AF African/African American [F]"]
       [:th "AF African/African American [M]"]
       [:th "AF East Asian"]
       [:th "AF East Asian [F]"]
       [:th "AF East Asian [M]"]
       [:th "AF Japanese"]
       [:th "AF Korean"]
       [:th "AF Other East Asian"]
       [:th "AF South Asian"]
       [:th "AF South Asian [F]"]
       [:th "AF South Asian [M]"]
       [:th "AF Ashkenazi Jewish"]
       [:th "AF Ashkenazi Jewish [F]"]
       [:th "AF Ashkenazi Jewish [M]"]
       [:th "AF Remaining"]
       [:th "AF Remaining [F]"]
       [:th "AF Remaining [M]"]]]
     [:tbody
      (for [row rows
            :let [{:keys [unit gene transcript amplicon primer chromosome start stop
                          seqf seqr type filter position reference alternate
                          af_raw af_male af_female af_popmax
                          af_nfe af_nfe_female af_nfe_male af_nfe_bgr af_nfe_nwe af_nfe_onf af_nfe_swe af_nfe_seu af_nfe_est
                          af_fin af_fin_female af_fin_male
                          af_amr af_amr_female af_amr_male
                          af_afr af_afr_female af_afr_male
                          af_eas af_eas_female af_eas_male af_eas_jpn af_eas_kor af_eas_oea
                          af_asj af_asj_female af_asj_male
                          af_sas af_sas_female af_sas_male
                          af_oth af_oth_female af_oth_male]} row]]
        [:tr {:replicant/key row
              :class (when (js/Number.isNaN position) "no-match")}
         [:td.no-wrap unit]
         [:td.center gene]
         [:td.center transcript]
         [:td.no-wrap amplicon]
         [:td.no-wrap primer]
         [:td.int.right chromosome]
         [:td.int.right (format-number start)]
         [:td.int.right (format-number stop)]
         [:td seqf]
         [:td seqr]
         [:td.center (if (= type "exome") "E" "G")]
         [:td.center (when (some #{"AC0"} filter) "✓")]
         [:td.int.right (format-number position)]
         [:td.right reference]
         [:td.right alternate]

         [:td.float (format-number af_raw)]
         [:td.float (format-number af_female)]
         [:td.float (format-number af_male)]
         [:td.float (format-number af_popmax)]

         [:td.float (format-number af_nfe)]
         [:td.float (format-number af_nfe_female)]
         [:td.float (format-number af_nfe_male)]
         [:td.float (format-number af_nfe_bgr)]
         [:td.float (format-number af_nfe_nwe)]
         [:td.float (format-number af_nfe_onf)]
         [:td.float (format-number af_nfe_swe)]
         [:td.float (format-number af_nfe_seu)]
         [:td.float (format-number af_nfe_est)]
         [:td.float (format-number af_fin)]
         [:td.float (format-number af_fin_female)]
         [:td.float (format-number af_fin_male)]

         [:td.float (format-number af_amr)]
         [:td.float (format-number af_amr_female)]
         [:td.float (format-number af_amr_male)]

         [:td.float (format-number af_afr)]
         [:td.float (format-number af_afr_female)]
         [:td.float (format-number af_afr_male)]

         [:td.float (format-number af_eas)]
         [:td.float (format-number af_eas_female)]
         [:td.float (format-number af_eas_male)]
         [:td.float (format-number af_eas_jpn)]
         [:td.float (format-number af_eas_kor)]
         [:td.float (format-number af_eas_oea)]

         [:td.float (format-number af_sas)]
         [:td.float (format-number af_sas_female)]
         [:td.float (format-number af_sas_male)]

         [:td.float (format-number af_asj)]
         [:td.float (format-number af_asj_female)]
         [:td.float (format-number af_asj_male)]

         [:td.float (format-number af_oth)]
         [:td.float (format-number af_oth_female)]
         [:td.float (format-number af_oth_male)]])]]))

(defn render-page [conn]
  (let [db (ds/db conn)
        state (ds/entity db :system/app)
        dataset (:dataset state)
        file-name (:file-name state)]
    [:div
     [:header
      [:h1 "Primer Check"]
      [:section#controls
       (upload-form state)
       (when dataset
         (download-button state))
       (when (and dataset file-name)
         [:div#processed-file
          [:span.dimmed "Current file: "] file-name])
       (process-indicator state)]]
     [:main
      [:section#content
       (when dataset
         ;; TODO Move elsewhere
         (.setItem js/localStorage "dataset" (tmd/dataset->data dataset))
         (data-view dataset))]]]))
