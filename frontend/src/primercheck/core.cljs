(ns primercheck.core
  (:require [clojure.walk :as walk]
            [datascript.core :as ds]
            [replicant.dom :as r]
            [primercheck.event :refer [interpolate-actions execute-actions]]
            [primercheck.sse]
            [primercheck.ui :as ui]))

(defn main [conn el]
  (add-watch
   conn ::render
   (fn [_ _ _ _]
     (r/render el (ui/render-page conn))))

  (r/set-dispatch!
   (fn [event-data actions]
       (->> actions
            (interpolate-actions
             (:replicant/dom-event event-data))
            (execute-actions event-data conn))))

  ;; Trigger the initial render
  (ds/transact! conn [{:db/ident :system/app
                       :app/started-at (js/Date.)}]))
