(ns primercheck.event
  (:require [clojure.string :as str]
            [clojure.walk :as walk]
            [datascript.core :as ds]
            [primercheck.event.upload :refer [upload-file]]
            [primercheck.event.download :refer [download-csv]]
            [primercheck.event.fetch :refer [fetch-result]]))

(defn interpolate-actions [js-event actions]
  (walk/postwalk
   (fn [x]
     (case x
       :event/target (-> js-event .-target)
       :event/target.value (-> js-event .-target .-value)
       :event/target.file (-> js-event .-target .-files (aget 0))
       ;; Add more cases as needed
       x))
    actions))

(defn execute-actions [{event :replicant/dom-event
                        node :replicant/dom-node} conn actions]
      (doseq [[action & args] actions]
        (case action
          ;; backend communication
          :http/upload-xlsx (upload-file event conn (first args) (second args))
          ;; app db interaction
          :db/transact (apply ds/transact! conn args)
          ;; dom manipulation
          :dom/disable (set! (-> event .-target .-disabled) true)
          :dom/enable (set! (-> event .-target .-disabled) false)
          :dom/prevent-default (-> event .preventDefault)
          :dom/set-input-text (set! (-> event .-target .-value) (first args))
          ;; ui control
          :ui/filter-column (js/console.log "filter column action")
          :ui/sort-column (js/console.debug "sort by column action")
          :ui/download-csv (download-csv (first args))
          (js/console.debug "Unknown action" action "with arguments" args))))
