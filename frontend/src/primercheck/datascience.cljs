(ns primercheck.datascience
  (:require
    [clojure.string :as str]
    [tech.v3.dataset :as tmd]))

(defn filter-by-gene [ds symbol]
  (tmd/filter-column ds :gene symbol))

(defn sorted-by-position [ds]
  (tmd/sort-by-column ds :position < {:nan-strategy :last}))

(defn transform-nans [ds]
  (->> (tmd/rows ds)
    (mapv #(reduce-kv (fn [m k v]
                        (cond
                          (js/Number.isNaN v) (assoc m k "")
                          :else (assoc m k v)))
                         {} %))
    (tmd/->dataset)))

(defn dataset->sorted [ds]
      (let [sorted-by-position (->> (tmd/group-by ds :amplicon)
                                    (map (fn [[k v]]
                                             (sorted-by-position v)))
                                    (map tmd/rows)
                                    (flatten))]
        (-> sorted-by-position
            tmd/->dataset
            (tmd/sort-by-column :amplicon <)
            tmd/rows)))
