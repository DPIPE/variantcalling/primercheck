(ns primercheck.datastore
    (:require  [datascript.core :as ds]))

(def schema {:system/app {:connected? false
                          :processing? false}})

(defonce conn (ds/create-conn schema))

(def app-db
      (fn [] (ds/entity (ds/db conn) :system/app)))
