(ns clean
  (:require [babashka.fs :as fs]))

(defn remove-compiled-js
  {:shadow.build/stage :compile-prepare}
  [build-state & args]
  (let [js-dir "public/js/compiled"]
    (when (fs/directory? js-dir)
      (fs/delete-tree js-dir)))
  build-state)
