#!/bin/bash

IMAGE_NAME="build-duckdb:temp"

if [ ! -f lib/duckdb-java/duckdb_jdbc.jar ]; then
    echo "Building DuckDB JDBC JAR from source. This will take a while..."
    mkdir -p ./lib/duckdb-java
    docker build -t "$IMAGE_NAME" -f ./docker/duckdb.dockerfile .
    docker create --name dummy "$IMAGE_NAME"
    docker cp dummy:/tmp/duckdb-java/build/release/duckdb_jdbc.jar ./lib/duckdb-java/duckdb_jdbc.jar
    docker rm -f dummy
    docker rmi -f "$IMAGE_NAME"
else
    echo "DuckDB JDBC JAR exists."
fi
