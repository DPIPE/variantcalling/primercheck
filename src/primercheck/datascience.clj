(ns primercheck.datascience
  (:require [tablecloth.api :as tc]
            [clojure.string :as str]))

(defn result->dataset
  "Process SQL results. We currently return two JSON fields with a
  collection of exomes/genomes that match reads. This is done, because
  there are differing number of columns between the two. Hence, massaging
  the data seems to be better done on the application level, rather than
  the datastore layer."
  [{:keys [unit primer exomes genomes] :as result}]
  (let [base-map (dissoc result :genomes :exomes)
        exome-maps (map #(merge base-map {:type "exome"} %) exomes)
        genome-maps (map #(merge base-map {:type "genome"} %) genomes)]
    (-> (concat exome-maps genome-maps)
        (tc/dataset {:name (str unit "-" primer)
                     :key-fn name})
        (tc/rename-columns #(-> % (str/replace #"info_" "") str/lower-case)))))

(defn only-high-quality-genotypes [ds]
  (-> ds
      (tc/select-rows #(some #{"AC0"} (% "filter")))))

(defn results->dataset [results]
  (let [datasets (map result->dataset results)]
    (tap> [::results->dataset datasets])
    (-> (apply tc/bind datasets))))
