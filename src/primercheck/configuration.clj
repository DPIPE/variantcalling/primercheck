(ns primercheck.configuration
  (:require [integrant.core :as ig]
            [aero.core :as aero]))

;; Extend aero with integrant reader
(defmethod aero/reader 'ig/ref
  [{:keys [profile] :as opts} tag value]
  (ig/ref value))
