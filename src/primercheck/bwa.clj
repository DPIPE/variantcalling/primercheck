(ns primercheck.bwa
  "Perform Burrows-Wheeler alignment of reads in FASTA format.
  See: [`bwa` Page](https://bio-bwa.sourceforge.net/)"
  (:require [clojure.string :as str]
            [clojure.pprint :refer [cl-format]]
            [clojure.core.match :refer [match]]
            [babashka.fs :as fs]
            [babashka.process :refer [process check]]
            [taoensso.telemere :as t]
            [cljam.io.sam :as sam]))

(def ^:private non-printable-regex #"\xa0")

(def ^:private column-order '(:unit :gene :transcript :amplicon :primer :chromosome :start :stop :seqf :seqr))

(defn explode-qname
  "We receive hash-maps of alignments from `cljam.io.sam`, containing
  only the keys: `rname`, `qname`, `pos`, and `end`. This funtion explodes
  the `:qname` value string into separate keys, which is useful for working
  with the resulting data after querying the GnomAD database. Also serves as
  more legible reference for the lab."
  [coll-of-alignments]
  (->> coll-of-alignments
       (map #(reduce-kv (fn [m k v]
                          (match k
                            :qname (merge m (zipmap [:unit :gene :transcript :amplicon :seqf :seqr :primer]
                                                    (str/split v #";")))
                            :else (assoc m k v))) {} %))
       (map #(update-keys % (fn [k] (match k
                                      :rname :chromosome
                                      :pos :start
                                      :end :stop
                                      :else k))))
       (map #(update-vals % (fn [v] (if (string? v) (-> v str/upper-case (str/replace non-printable-regex "")) v))))
       (map #(apply array-map (interleave column-order (map % column-order))))))

(defn sam->alignments
  "Reads SAM file into alignment maps.

  This operation discards reads for which RNEXT is not available."
  [f]
  (let [alignments (with-open [r (sam/sam-reader (fs/file f))]
                     (doall (sam/read-alignments r)))]
    (->> alignments
         (map (comp
               #(select-keys % [:rname :qname :pos :end])
               #(update-keys % (comp keyword name))))
         (filter #(not= (:rname %) "*")))))

;; TODO Pass in reference fasta to use, e.g. human_g1k_v37_decoy.fasta
(defn fasta->alignments
  "Map and convert FASTA to alignment maps.

  The key `fasta-string` expects a string of TAB-delimited FASTA entries,
  parsed from user input, e.g. `xlsx` or `csv` formats. NB! The string can
  contain multiple reads.

  The key `reference-fasta` expects a string containing the relative path to
  a reference genome FASTA file, within which an index (`fai`) is expected to exist.

  The `fasta-string` is then sent through two external commands to:

  - Map SA coordinates via Burrows-Wheeler alignment tool ([bwa](https://github.com/lh3/bwa))
  - Generate alignments in SAM format: `bwa samse`

  Finally, read alignments from SAM file into sequence of maps on the form:

  ```clojure
  '({:rname <string> :qname <string> :pos <integer> :end <integer>} ... )
  ```

  These are used as constraints when querying DuckDB."
  [{:keys [fasta-string reference-fasta]}]
  (fs/with-temp-dir [d nil]
    (let [temp-fasta (fs/create-temp-file {:dir d :suffix ".fasta"})
          temp-sam (fs/create-temp-file {:dir d :suffix ".sam"})]
      (fs/write-bytes temp-fasta (.getBytes fasta-string))
      (-> (process {:pre-start-fn #(t/log! :debug {:cmd (cl-format nil "Running ~a" (str/join " " (:cmd %)))})
                    :dir "data/gatk"}
                   "bwa aln -o 0 -n 1 -l 100" reference-fasta temp-fasta)
          (process {:pre-start-fn #(t/log! :debug {:cmd (cl-format nil "Running ~a" (str/join " " (:cmd %)))})
                    :dir "data/gatk"
                    :in :inherit
                    :out :write
                    :out-file temp-sam}
                   "bwa samse" reference-fasta "-" temp-fasta)
          check)
      (->> (sam->alignments temp-sam)
           explode-qname))))
