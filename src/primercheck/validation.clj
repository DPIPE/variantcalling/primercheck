(ns primercheck.validation
  (:require [malli.core :as m]
            [malli.transform :as mt]
            [clojure.string :as str]
            [malli.dev.pretty :as pretty]
            [clojure.test.check.generators :as gen]
            [malli.generator :as mg]
            [malli.error :as me]
            [dk.ative.docjure.spreadsheet :as xlsx]))

;;;;;;;;;;
;; XLSX ;;
;;;;;;;;;;

(def column-names
  ["Enhet"
   "GEN"
   "Transkript(iSeqPilot)"
   "Amplikonnavn"
   "Primer-Fnavn"
   "Primersekvens_F(5’-->3’)"
   "Retning"
   "Primer-Rnavn"
   "Primersekvens_R(5’-->3’)"
   "Retning"])

(def all-columns-map
  {:A :unit
   :B :gene
   :C :transcript
   :D :amplicon
   :E :primer-f
   :F :seq-f
   :G :direction-f
   :H :primer-r
   :I :seq-r
   :J :direction-r})

(def xlsx-columns
  [:and
   [:sequential {:min 10 :max 10} string?]
   [:fn {:error/message {:en (str "expected columns: " (apply str (interpose ", " column-names)))}}
    (fn [coll] (= coll column-names))]])

(defn get-column-names [sheet]
  (->> sheet
       (xlsx/row-seq)
       (first)
       (xlsx/cell-seq)
       (map xlsx/read-cell)))

(defn invalid-xlsx-columns? [workbook]
  (let [sheet-name (-> workbook xlsx/sheet-seq first xlsx/sheet-name)
        sheet (xlsx/select-sheet sheet-name workbook)]
    (me/humanize (m/explain xlsx-columns (get-column-names sheet)))))

(def valid-xlsx-columns? (complement invalid-xlsx-columns?))

(defn validate-xlsx-workbook [workbook]
  (when-not (xlsx/workbook? workbook)
    (throw
     (ex-info "Not an xlsx workbook"
              {:cause :not-a-workbook
               :description ["not a workbook"]})))
  (let [invalid-cols (invalid-xlsx-columns? workbook)]
    (when invalid-cols
      (throw
       (ex-info "Invalid xlsx columns"
                {:cause :invalid-xlsx-columns
                 :description invalid-cols})))))


;;;;;;;;;;;
;; FASTA ;;
;;;;;;;;;;;

;;;;;;;;;;;;;;;;
;; ALIGNMENTS ;;
;;;;;;;;;;;;;;;;
