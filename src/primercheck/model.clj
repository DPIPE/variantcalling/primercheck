(ns primercheck.model
  (:require [honey.sql :as sql]
            [next.jdbc :as jdbc]
            [taoensso.telemere :as t]))

(defn find-gnomad-matches [{:keys [datasource constraints] :as req}]
  (let [{:keys [chromosome start stop]} constraints
        query (sql/format
               {:with [[[:alignment {:columns [:chromosome :start :stop]}]
                        {:values [[chromosome start stop]]}]]
                :select [[[:raw "to_json(list_distinct(list(e) filter (e.chromosome is not null))) as exomes"]]
                         [[:raw "to_json(list_distinct(list(g) filter (g.chromosome is not null))) as genomes"]]]
                :from [[:alignment :a]]
                :left-join [[:exomes_v2 :e] [:and
                                             [:= :e.chromosome :a.chromosome]
                                             [:between :e.position :a.start :a.stop]]
                            [:genomes_v2 :g] [:and
                                              [:= :g.chromosome :a.chromosome]
                                              [:between :g.position :a.start :a.stop]]]
                :group-by :all}
               {:params constraints})]
    (try
      (jdbc/execute-one! datasource query)
      (catch Exception e
        (t/log! :error e)))))
