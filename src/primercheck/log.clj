(ns primercheck.log
  (:require [clojure.pprint :refer [cl-format]]
            [integrant.core :as ig]
            [taoensso.telemere :as t]
            [taoensso.telemere.timbre :as tt]
            [taoensso.telemere.tools-logging :refer [tools-logging->telemere!]]))

;; log uncaught exceptions in threads
(Thread/setDefaultUncaughtExceptionHandler
 (reify Thread$UncaughtExceptionHandler
   (uncaughtException [_ thread ex]
     (t/log! :error {:what :uncaught-exception
                     :exception ex
                     :where (str "Uncaught exception on" (.getName thread))}))))

(def ^:private minimum-log-level (if (System/getProperty "dev") :debug :info))

(defn init! [{:keys [disallowed-ns] :or {disallowed-ns ["some.noisy.namespace.*"]} :as config}]
  (tt/set-min-level! minimum-log-level)
  ;; TODO Not implemented, it appears. Might not be needed
  ;;(t/set-ns-filter! {:disallow disallowed-ns})
  (tools-logging->telemere!)
  (t/log! :info (cl-format nil "Started log facility. Log level ~a" minimum-log-level)))

(defmethod ig/init-key :module/logging
  [_ {:keys [logger] :as config}]
  (init! logger))
