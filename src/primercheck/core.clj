(ns primercheck.core
  (:gen-class)
  (:require [clojure.java.io :as io]
            [integrant.core :as ig]
            [aero.core :as aero :refer [read-config]]
            [primercheck.log]
            [primercheck.configuration]
            [primercheck.communication]
            [primercheck.communication.handler]
            [primercheck.communication.router]
            [primercheck.datastore]))

(def ^:private config (read-config (io/resource "config.edn")))

(defn -main [& args]
  (ig/init config))
