(ns primercheck.communication
  (:require [clojure.pprint :refer [cl-format]]
            [integrant.core :as ig]
            [taoensso.telemere :as t]
            [ring.adapter.jetty9 :refer [run-jetty]])
  (:import (java.util.concurrent Executors)
           (org.eclipse.jetty.util.thread QueuedThreadPool)
           (java.util.concurrent Executors)))

(defmethod ig/init-key :module/communication  [_ {:keys [port handler]}]
  (t/log! :info (cl-format nil "Starting communication server on port ~a." port))
  (let [thread-pool (new QueuedThreadPool)
        _ (.setVirtualThreadsExecutor thread-pool
                                      (Executors/newVirtualThreadPerTaskExecutor))]
    (run-jetty handler
               {:port port
                :join? false
                :async? false
                :allow-null-path-info true
                :thread-pool thread-pool})))

(defmethod ig/halt-key! :module/communication [_ server]
  (t/log! :info "Stopping communication server.")
  (.stop server))
