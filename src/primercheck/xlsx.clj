(ns primercheck.xlsx
  (:require [clojure.string :as str]
            [clojure.pprint :refer [cl-format]]
            [dk.ative.docjure.spreadsheet :as xlsx]
            [primercheck.validation :as validation]))

;; TODO Consider moving to EDN,
(def columns-map
  {:A :unit
   :B :gene
   :C :transcript
   :D :amplicon
   :E :primer-f
   :F :seq-f
   :H :primer-r
   :I :seq-r})

(defn first-sheet-name [workbook]
  (-> workbook xlsx/sheet-seq first xlsx/sheet-name))

(defn xlsx-rows->maps [workbook]
  (->> (xlsx/select-sheet (first-sheet-name workbook) workbook)
       (xlsx/select-columns columns-map)
       rest
       distinct))

(defn xlsx->fasta
  "Convert first sheet from excel file to FASTA strings.

  A note on `cl-format`. This macro is based on Commmon Lisp's format macro.
  The template string may look unintelligible, however, it allows for a terse
  way of formatting strings, as seen in use below. Here are some explanations
  of what the template expresses:

  - `~&`: inserts a newline unless we're already at the beginning of a line.
  - `~@{...~}` processes the arguments iteratively.
  - `~:[...~;...~]` chooses between the nil and non-nil case.
  - `~:*` unconsumes the argument that was consumed by `~:[...~]`.
  - `~A`: outputs the item being processed.
  - `~^`: escapes from the loop on the last iteration (so as not to output an excessive space after the last item).

  Such that:

  ```clojure
  (cl-format nil \">~@{~:[~;~:*~A~];~}\" unit gene transcript ...)
  ```

  takes the list of bindings and iterates through it, interpolates each value into the template, and substituting
  any `nil`s with an empty string, e.g. empty transcript entries.
  "
  [xlsx-file & opts]
  (let [workbook (xlsx/load-workbook xlsx-file)]
    (if (and
         (xlsx/workbook? workbook)
         (validation/valid-xlsx-columns? workbook))
      (->> workbook
           (xlsx-rows->maps)
           (map #(update-vals % (fn [v] (when v (str/trim v)))))
           (map (fn [{:keys [unit gene transcript amplicon primer-f seq-f primer-r seq-r]}]
                  (let [header (cl-format nil ">~@{~:[~;~:*~A~];~}" unit gene transcript amplicon seq-f seq-r)]
                    (cl-format nil "~@{~a~a~&~a~&~}" header primer-f seq-f header primer-r seq-r))))
           (str/join))
      (validation/validate-xlsx-workbook workbook))))
