(ns primercheck.communication.api
  (:require [clojure.core.async :as async]
            [clojure.pprint :refer [cl-format]]
            [embroidery.api :as vt]
            [dk.ative.docjure.spreadsheet :as xlsx]
            [primercheck.bwa :as bwa]
            [primercheck.datascience :as datascience]
            [primercheck.model :as model]
            [primercheck.communication.sse :as sse]
            [primercheck.xlsx :as primercheck.xlsx]
            [taoensso.telemere :as t]
            [primercheck.validation :as validation]))

(defn ^:private parse-xlsx [content-type filename size tempfile]
  (if-not tempfile
    (throw
     (ex-info "Missing file."
              {:causes :missing-file
               :file {:filename filename :content-type content-type :size size}}))
    (try
      (primercheck.xlsx/xlsx->fasta tempfile)
      (catch Exception e
        (throw
         (ex-info (.getMessage e)
                  {:causes :invalid-xlsx
                   :file {:filename filename :content-type content-type :size size}}))))))

(defn ^:private process-fasta [fasta-string]
  (try
    (bwa/fasta->alignments {:fasta-string fasta-string :reference-fasta  "human_g1k_v37_decoy.fasta"})
    (catch Exception e
      (throw
       (ex-info (.getMessage e)
                {:cause :failed-alignment})))))

(defn ^:private process-alignments [client-id datasource alignments]
  (let [results (doall (vt/pmap*
                        (fn [alignment]
                          (let [processed (->> (model/find-gnomad-matches
                                                {:datasource datasource
                                                 :constraints alignment})
                                               (merge alignment))]
                            processed))
                        alignments))
        filtered-results (->> results
                              (filterv #(some (fn [pred] (pred %))
                                                          [(comp (complement nil?) :exomes)
                                                           (comp (complement nil?) :genomes)])))
        dataset (datascience/results->dataset filtered-results)]
    (swap! sse/results assoc client-id dataset)))

(defn process-upload [{{{:keys [content-type filename size tempfile]} "file"} :multipart-params
                       session :session
                       cookies :cookies :as req}]
  (let [_  (tap> [::req req])
        client-id (get session :client-id)
        ch (get @sse/clients client-id (async/chan))
        datasource (:datasource req)
        fasta-string (parse-xlsx content-type filename size tempfile)
        alignments (process-fasta fasta-string)
        _ (sse/send>!! ch (sse/format-event "processing-status" {:status :running}))]
    (process-alignments client-id datasource alignments)
    (sse/send>!! ch (sse/format-event "processing-status" {:status :done
                                                           :uri (cl-format nil "/fetch?id=~a" (str client-id))}))))

(def ^:private request-queue (async/chan 1000))

(defn start-request-queue []
  (t/log! :info "Starting request queue.")
  (async/go-loop []
    (let [new-request (async/<! request-queue)]
      (process-upload new-request)
      (recur))))

(defn handle-upload [{{{:keys [content-type filename size tempfile]} "file"} :multipart-params :as req}]
  (try
    (xlsx/load-workbook tempfile)
    (do (async/go (async/>! request-queue req))
      {:status 202
       :body ""})
    (catch Exception e
      {:status 400
       :body {:error/message (.getMessage e)
              :error/cause :invalid-file-type}})))

(defn handle-fetch [{:parameters/keys [query]
                     session :session
                     cookies :cookies :as req}]
  (let [client-id (get session :client-id)
        result (get @sse/results client-id)]
    (if result
      {:status 200
       :body result}
      {:status 404
       :body {:error/message "No result available"}})))
