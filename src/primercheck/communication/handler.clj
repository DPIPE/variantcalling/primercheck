(ns primercheck.communication.handler
  (:require [integrant.core :as ig]
            [taoensso.telemere :as t]
            [primercheck.communication.router :refer [get-routes]]
            [primercheck.communication.api :as api]))

(defmethod ig/init-key :module/handler [_ settings]
  (t/log! :debug settings)
  (t/log! :info "Initialising communication handler.")
  (api/start-request-queue)
  (get-routes settings))
