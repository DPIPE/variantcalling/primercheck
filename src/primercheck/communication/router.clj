(ns primercheck.communication.router
  (:require [reitit.ring.malli :refer [temp-file-part]]
            [reitit.ring :as ring]
            ;;[reitit.ring.middleware.defaults :refer [defaults-middleware]]
            [ring.middleware.defaults :refer [site-defaults]]
            [ring.middleware.session.memory :as memory]
            [ring.middleware.cors :refer [wrap-cors]]
            [ring.middleware.cookies :refer [wrap-cookies]]
            ;; [ring.middleware.anti-forgery :refer [wrap-anti-forgery]]
            [ring.middleware.session :refer [wrap-session]]
            [reitit.coercion.malli]
            [reitit.ring.coercion :as coercion]
            [reitit.ring.middleware.multipart :as multipart]
            [reitit.ring.middleware.muuntaja :as muuntaja]
            [reitit.ring.middleware.exception :as exception]
            [reitit.ring.middleware.parameters :as parameters]
            [reitit.dev.pretty :as pretty]
            [muuntaja.middleware :as mw]
            [malli.util :as mu]
            [taoensso.telemere :as t]

            [primercheck.communication.middleware :as primercheck.middleware]
            [primercheck.model :as model]
            [primercheck.communication.sse :as sse]
            [primercheck.communication.api :as api]))

(def ^:private session-store (memory/memory-store))

(defn get-routes [{:keys [datasource] :as settings}]
  (ring/ring-handler
   (ring/router
    ["" {:middleware [#(wrap-cors % :access-control-allow-origin [(re-pattern (get-in settings [:http :allow-origin]))]
                                    :access-control-allow-credentials "true"
                                    :access-control-allow-methods [:get :post :options])]}
     ["/event" #'sse/handler]
     ["/upload" {:post {:parameters {:multipart {:file temp-file-part}}
                        :handler #'api/handle-upload}}]
     ["/fetch" {:get {:parameters {:query {:id uuid?}}}
                :handler #'api/handle-fetch}]
     ["/ping" (constantly {:status 200, :body "pong"})]]
    {    ;;:exception pretty/exception
     :data { ;;:coercion reitit.coercion.malli/coercion
            :muuntaja primercheck.middleware/muuntaja-instance
            :middleware [
                         [primercheck.middleware/wrap-datasource datasource]
                         [primercheck.middleware/inject-data (:s3/credentials settings)]
                         ;;wrap-anti-forgery
                         [wrap-cookies]
                         [wrap-session {:store session-store
                                        :cookie-name "primercheck-session"}]
                         parameters/parameters-middleware
                         muuntaja/format-negotiate-middleware
                         muuntaja/format-response-middleware
                         muuntaja/format-request-middleware
                         primercheck.middleware/wrap-exceptions
                         coercion/coerce-response-middleware
                         coercion/coerce-request-middleware
                         multipart/multipart-middleware]
            :defaults (-> site-defaults
                          (assoc-in [:session :store] session-store))}})
   (ring/routes
    (ring/create-resource-handler {:path "/"})
    (ring/redirect-trailing-slash-handler {:method :strip})
    (ring/create-default-handler
     {:not-found (constantly {:status 404 :body "Not found"})}))))
