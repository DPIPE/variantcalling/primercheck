(ns primercheck.communication.sse
  (:require [clojure.set :refer [map-invert]]
            [clojure.pprint :refer [cl-format]]
            [clojure.core.async :as async]
            [clojure.java.io :as io]
            [ring.core.protocols :refer [StreamableResponseBody]]
            [ring.util.request :refer [path-info]]
            [clojure.core.async.impl.channels]
            [charred.api :as j])
  (:import (java.io OutputStream)))

;; Extend core.async channel with StreamableResponseBody
(extend-type clojure.core.async.impl.channels.ManyToManyChannel
  StreamableResponseBody
  (write-body-to-stream [ch _response ^OutputStream output-stream]
    (with-open [out output-stream
                writer (io/writer out)]
      (try
        (loop []
          (when-let [^String msg (async/<!! ch)]
            (doto writer (.write msg) (.flush))
            (recur)))
        ;; If the client disconnects writing to the output stream
        ;; throws an IOException.
        (catch java.io.IOException _)
        ;; Close channel after client disconnect.
        (finally (async/close! ch))))))


(defn ^:private counter []
  (let [v (atom 0)]
    (fn [] (swap! v inc'))))

;; defonce
(def ^:private message-id (counter))

;; TODO Use datalevin for persisting results to disk as kv-store
(def results (atom {}))

;; TODO Change to defonce
(def clients (atom {}))

(defn format-event [event-name data]
      (cl-format nil "id: ~a~%event: ~a~%data: ~a~%~%"
                 (message-id)
                 event-name
                 (j/write-json-str data)))

(defn format-data-event [data]
  (cl-format nil "data: ~a~%~%" (j/write-json-str data)))

;; Using protocols is faster, but we are not concerned with speed at
;; this point, so multimethods will do... I think.
;; What we want is to 'defer' the swap operation to add new channels
;; to the clients map, such that we dont encounter a 500 on page refresh.
(derive clojure.core.async.impl.channels.ManyToManyChannel ::asyncchan)
(derive java.util.UUID ::uuid)

(defmulti send>!! (fn [ch-or-client-id _] [(class ch-or-client-id)]))

(defmethod send>!! [::asyncchan] [ch message]
  (let [v (async/>!! ch message)
        client-id (get (map-invert @clients) ch)]
    ;;(tap> [::send>!! {client-id v}])
    (when-not v (swap! clients dissoc client-id))
    v))


(defmethod send>!! [::uuid] [client-id message]
  (let [ch (get @clients client-id)
        v (async/>!! ch message)]
    (when-not v (swap! clients dissoc client-id))
    ;; keeps return semantics for >!!
    v))

(defn heartbeat>!! [ch msec]
  (Thread/startVirtualThread
   #(loop []
              (Thread/sleep ^long msec)
              (when (send>!! ch "\n\n")
                (recur)))))

(defn get-cookie [cookies]
  (get-in cookies ["ring-session" :value]))

(defn client-exists? [client-id]
  (instance? clojure.core.async.impl.channels.ManyToManyChannel (get @clients client-id)))

;; TODO Change setting cookie in response!
(defn handler [{:keys [cookies session] :as req}]
  (let [path (path-info req)
        cookie (get-cookie cookies)
        client-id (get session :client-id (random-uuid))
        ch (get @clients client-id (async/chan 10))]
    (tap> [::handler {:path path :cookie cookie :client-id client-id :request req}])
    (when-not (client-exists? client-id)
      (swap! clients assoc client-id ch)
      (heartbeat>!! ch 6000)
      ;;(send>!! ch (cl-format nil "event: server-message~%retry: 360000~%~%"))
      {:status 200
       :headers {"Content-Type" "text/event-stream"
                 "Cache-Control" "no-cache, no-store"}
       :body ch
       :session {:client-id client-id}})
    {:status 200
     :headers {"Content-Type" "text/event-stream"
               "Cache-Control" "no-cache, no-store"}
     :body ch
     :session {:client-id client-id}}))

(comment
  @clients
  (send>!! (last (keys @clients))
           (format-event
            "processing-status"
            {:status "done"
             :uri (str "/fetch?id=" (last (keys @clients)))}))



  )
