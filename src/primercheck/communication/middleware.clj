(ns primercheck.communication.middleware
  (:require [muuntaja.core :as m]
            [reitit.ring.middleware.exception :as exception]
            [taoensso.telemere :as t]))

(defrecord PrimercheckData [])

;; Hide data when printing to stdout
(defmethod clojure.core/print-method PrimercheckData [data ^java.io.Writer writer]
  (.write writer "#<PrimercheckData"))

(defn inject-data [handler data]
  (let [data (map->PrimercheckData data)]
    (fn [request]
      (handler (assoc request :data data)))))

(derive ::error ::exception)
(derive ::failure ::exception)

(defn exception-handler [message exception request]
  (let [body {:type message
              :message (ex-message exception)
              :exception (str (.getClass exception))
              :uri (request :uri)}
        stacktrace (apply str (interpose \n (.getStackTrace exception)))]
    (t/log! :error (assoc body
                          :identity (request :identity)
                          :stacktrace stacktrace
                          :data (ex-data exception)))
    (t/log! :debug stacktrace)
    {:status 500
     :body body}))

(def muuntaja-instance
  (-> m/default-options
      (update-in [:formats] dissoc "application/transit+msgpack" "application/edn")
      (assoc :charsets #{"utf-8"})
      (m/create)))

(defn wrap-datasource [handler ds]
  (fn [req]
    (handler (assoc req :datasource ds))))


(def wrap-exceptions
  (exception/create-exception-middleware
   (merge exception/default-handlers
          { ;; ex-data = error
           ::error (partial exception-handler "error")
           ;; ex-data = exception or failure
           ::exception (partial exception-handler "exception")
           ;; overwrite default
           ::exception/default (partial exception-handler "default")})))

(defn wrap-debug-inject [handler data]
  (fn [request]
    (handler (merge request data))))
