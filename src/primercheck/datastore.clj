(ns primercheck.datastore
  (:require [clojure.pprint :refer [cl-format]]
            [integrant.core :as ig]
            [taoensso.telemere :as t]
            [next.jdbc :as jdbc]
            [next.jdbc.result-set :as rs]
            [charred.api :as j])
  (:import org.duckdb.JsonNode))

(def ^:private <-json #(j/read-json % {:key-fn keyword}))
(def ^:private ->json #(j/write-json-str %))

;; (defn ->duckjson
;;   "Transforms Clojure data to a JsonNode."
;;   [x]
;;   (let [pgtype (or (:pgtype (meta x)) "jsonb")]
;;     (doto (PGobject.)
;;       (.setType pgtype)
;;       (.setValue (->json x)))))

(defn ^:private <-duckjson
  "Transform JsonNode value to Clojure data."
  [^JsonNode v]
  (let [type (type v)
        value (str v)]
    (if (#{org.duckdb.JsonNode} type)
      (some-> value <-json (with-meta {:duckdbtype type}))
      value)))

;; if a row contains a JsonNode then we'll convert them to Clojure data
;; while reading:
(extend-protocol rs/ReadableColumn
  org.duckdb.JsonNode
  (read-column-by-label [^org.duckdb.JsonNode v _]
    (<-duckjson v))
  (read-column-by-index [^org.duckdb.JsonNode v _2 _3]
    (<-duckjson v)))

(defmethod ig/init-key :module/datastore
  [_ {:keys [dbtype dbname] :as config}]
  (let [ds (jdbc/get-datasource {:dbtype dbtype :dbname dbname "duckdb.read_only" true})]
    (t/log! :info "Datastore connection established.")
    ds))

(defmethod ig/halt-key! :module/datastore
  [_ ds]
  (.close (jdbc/get-connection ds))
  (t/log! :info "Datastore disconnected."))
