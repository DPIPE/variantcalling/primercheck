## Primer Check

### Description
A full stack Clojure application to help the lab discover allele dropouts.

The repo contains both Clojure source code to run a back end HTTP server,
and a front end application.

### License
This work is multi-licensed under Eclipse Public License 2.0 and GNU Public License 3.0 (or any later version).
The EPL-2.0 license accounts for usage of libraries licensed under EPL-1.0, which is not compatible with GPL-3.0-or-later.
GPL-3.0-or-later is used for all code not under EPL-1.0, i.e. code that is "non-derivative works".

`SPDX-License-Identifier: EPL-2.0 OR GPL-3.0-or-later`
