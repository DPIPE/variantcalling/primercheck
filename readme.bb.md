## Babashka Tasks

### `fetch_gnomad.clj`
A script meant to be run as a Babashka task when initialising the
application, since we need to access GnomAD data to run alignment
queries.

#### Stats
##### Hetzner
| Dataset | Download Time Spent | Disk Usage (BGzip VCF) | Disk Usage (Parquet) | Disk Usage (DuckDB) |
|---------|---------------------|------------------------|----------------------|---------------------|
| v2      | 01:55:03            | 512G                   | 122G                 | 16G                 |
| v4      | 02:21:07            | 710G                   | TBA                  |                     |

Note that resulting Parquet file size will be much less, since most
fields in VCFs will be filtered out, as they are not necessary for our purposes.

### `fetch_gatk.clj`
A script for downloading and indexing reference fasta file.
#### Stats
| Fasta                     | Indexing Time |
|---------------------------|---------------|
| human_g1k_v37_decoy.fasta | 00:40:00      |
