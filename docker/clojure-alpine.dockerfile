FROM eclipse-temurin:23-jdk-alpine

RUN apk add --no-cache \
    rlwrap \
    curl \
    tar \
    git \
    bash \
    && mkdir /app

SHELL ["/bin/bash", "-c"]

WORKDIR /tmp
RUN curl -L -O https://github.com/clojure/brew-install/releases/latest/download/posix-install.sh \
    && chmod +x posix-install.sh \
    && ./posix-install.sh \
    && rm ./posix-install.sh

CMD ["/bin/bash"]
