FROM clojure-alpine:latest

RUN apk add --no-cache build-base zlib-dev cmake ninja openssl-dev

WORKDIR /tmp

RUN git clone https://github.com/duckdb/duckdb-java.git \
    && cd duckdb-java \
    && make release

CMD /bin/bash
