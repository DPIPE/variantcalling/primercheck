FROM clojure-alpine:latest

RUN apk add --no-cache nodejs

WORKDIR /app
COPY frontend/ /app/

RUN adduser -D -u 66666 -s /bin/bash -h /app appuser \
    && chown -R appuser:appuser /app

USER appuser

RUN curl -fsSL https://bun.sh/install | bash

ENV BUN_INSTALL="/app/.bun"
ENV PATH="$BUN_INSTALL/bin:$PATH"

RUN clj -A:serve -P \
    && bun i -g shadow-cljs \
    && bun i \
    && shadow-cljs -A:release release :app

CMD clj -M:serve :port 8787 :dir public :headers '{"Cross-Origin-Opener-Policy" "same-origin"}'
