FROM clojure-alpine:latest

RUN apk add --no-cache build-base zlib-dev cmake ninja openssl-dev \
    && adduser -D -u 66666 -s /bin/bash -h /app appuser \
    && chown -R appuser:appuser /app

WORKDIR /tmp
RUN git clone https://github.com/lh3/bwa.git \
    && cd bwa \
    && make \
    && mv ./bwa /usr/local/bin \
    && cd /tmp && rm -rf bwa

USER appuser

WORKDIR /app
COPY deps.edn /app
RUN clj -P

COPY src/ /app/src
COPY resources/ /app/resources

CMD ["clj", "-M", "-m", "primercheck.core"]
