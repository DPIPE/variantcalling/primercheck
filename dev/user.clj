(ns user
  (:require [clojure.repl.deps :refer [add-lib add-libs sync-deps]]
    [clojure.java.io :as io]
    [integrant.core :as ig]
    [integrant.repl :refer [go halt prep init reset reset-all]]
    [aero.core :as aero :refer [read-config]]
    [portal.api :as p]

    ;; test data analysis
    [tablecloth.api :as tc]

    [primercheck.log]
    [primercheck.configuration]
    [primercheck.communication]
    [primercheck.communication.handler]
    [primercheck.communication.router]
    [primercheck.datastore]
    [clojure.edn :as edn]
    [clojure.string :as str]))

(def config (read-config (io/resource "config.edn") {:profile :dev}))

(integrant.repl/set-prep! #(ig/expand config))

(comment
  ;; peek at loaded config-map
 config

  ;; Control app start/halt/refresh during development
 (prep)
 (init)
 (go)
 (halt)
 (reset)
 (reset-all)

  ;; Open portal web ui for inspecting data via tap>
 (def p (p/open {:port 59645 :theme :portal.colors/zerodark}))
 (add-tap #'p/submit)

 )
