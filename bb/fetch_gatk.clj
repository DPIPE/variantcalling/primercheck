#!/usr/bin/env bb
(ns fetch-gatk
  (:require [babashka.fs :as fs]
            [babashka.curl :as curl]
            [babashka.process :refer [check process]]
            [clojure.java.io :as io]
            [clojure.tools.logging :as log]
            [clojure.string :as str]
            [clojure.edn :as edn]
            [clojure.pprint :refer [cl-format]])
  (:import java.util.zip.GZIPInputStream))

(defn download-fasta
  "Fetch data from Broad Institute's GATK bundle."
  [uri target-dir]
  (let [infile (-> uri (str/split #"/") last)
        outfile (-> (fs/split-ext infile) first)]
    (when-not (fs/exists? (str target-dir "/" outfile))
      (log/info "Downloading and uncompressing GATK data:" infile)
      (try
        (with-open [input (-> (curl/get uri {:as :stream}) :body io/input-stream GZIPInputStream.)
                    output (io/output-stream (str target-dir "/" outfile))]
          (io/copy input output))
        (catch Exception e (log/error (.getMessage e))))
      (log/info "File" infile "downloaded and decompressed into directory" target-dir)
      outfile)))

(defn index-fasta [fasta-file target-dir]
  (-> (process {:pre-start-fn #(log/info (cl-format nil "Running ~a" (str/join " " (:cmd %))))
                :dir target-dir}
               "bwa index" fasta-file)
      check)
  (log/info "Done indexing fasta file:" fasta-file))

(defn -main [& args]
  (let [{:gatk/keys [uri target-dir]} (-> (slurp "resources/config.bb.edn") edn/read-string)
        fasta-file (download-fasta uri target-dir)]
    (when fasta-file (index-fasta fasta-file target-dir))))
