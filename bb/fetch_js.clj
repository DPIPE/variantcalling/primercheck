(ns fetch-js
  (:require [babashka.fs :as fs]
            [babashka.http-client :as http]
            [clojure.java.io :as io]
            [clojure.pprint :refer [cl-format]]
            [clojure.tools.logging :as log]
            [clojure.edn :as edn]
            [clojure.string :as str]))

(defn download-htmx [{htmx-uri :htmx/uri
                      target-dir :htmx/target-dir
                      sse-uri :htmx-sse/uri}]
  (fs/create-dirs target-dir)
  (doseq [uri (list htmx-uri sse-uri)
          :let [outfile (last (str/split uri #"/"))
                _  (log/info "Downloading" outfile)]]
    (try
      (with-open [input (-> (http/get uri {:as :stream}) :body io/input-stream)
                  output (io/output-stream (cl-format nil "~a/~a" target-dir outfile))]
        (io/copy input output))
      (catch Exception e (log/error (.getMessage e))))))

(defn -main [& args]
  (let [config (-> (slurp "resources/config.bb.edn") edn/read-string)]
    (download-htmx config)))
