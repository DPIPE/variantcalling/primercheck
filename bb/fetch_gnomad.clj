#!/usr/bin/env bb
(ns fetch-gnomad
  (:require [babashka.fs :as fs]
            [babashka.curl :as curl]
            [babashka.cli :as cli]
            [clojure.tools.logging :as log]
            [clojure.java.io :as io]
            [clojure.pprint :refer [cl-format]]
            [clojure.string :as str]))

(def cli-options {:target-dir {:default "/storage/development/data/gnomad/vcf"}})

(defn gnomad-urls []
  (let [v2-uri "https://gnomad-public-us-east-1.s3.amazonaws.com/release/2.1.1/vcf/~a/gnomad.~a.r2.1.1.sites.~a.vcf.bgz"
        v4-uri "https://gnomad-public-us-east-1.s3.amazonaws.com/release/4.1/vcf/~a/gnomad.~a.v4.1.sites.chr~a.vcf.bgz"]
    (->> (conj (mapv str (range 1 23)) "X" "Y")
         (map #(list (cl-format nil v2-uri "genomes" "genomes" %)
                         (cl-format nil v2-uri "exomes" "exomes" %)
                         (cl-format nil v4-uri "genomes" "genomes" %)
                         (cl-format nil v4-uri "exomes" "exomes" %)))
         flatten)))

(defn already-downloaded? [target-dir]
  (let [gnomad-files (map #(last (str/split % #"/")) (gnomad-urls))
        v2 (filter #(not (fs/exists? (str target-dir "/v2/" %))) (filter #(str/includes? % "2.1.1") gnomad-files))
        v4 (filter #(not (fs/exists? (str target-dir "/v4/" %))) (filter #(str/includes? % "4.1") gnomad-files))]
    (= [v2 v4] ['("gnomad.genomes.r2.1.1.sites.Y.vcf.bgz") '()])))

;; TODO Place files in respective version subdirs, e.g. v2, vn...
(defn download-gnomad-data
  "Download GnomAD BGzip VCFs in parallel."
  [target-dir]
  (when-not (already-downloaded? target-dir)
    (log/info "Downloading GnomAD data.")
    (doall
     (pmap (fn [url]
             (let [outfile (last (str/split url #"/"))
                   outpath (str target-dir "/" outfile)]
               (try
                 (with-open [input (-> (curl/get url {:as :stream}) :body io/input-stream)
                             output (io/output-stream outpath)]
                   (io/copy input output)
                   (log/info "Downloaded" outpath))
                 (catch Exception e (log/warn "Failed" outfile (.getMessage e))))))
           (gnomad-urls)))
    (log/info "Done downloading GnomAD data.")))

(defn -main [& args]
  (let [{:keys [target-dir]} (cli/parse-opts *command-line-args* {:spec cli-options})]
    (download-gnomad-data target-dir)))
